package com.palup;

import com.google.inject.AbstractModule;
import com.palup.dao.EventDAO;
import com.palup.resources.PalUpResource;
import com.palup.stores.DataStore;
import com.palup.stores.MySqlStore;
import io.dropwizard.setup.Environment;
import org.skife.jdbi.v2.DBI;

/**
 * @author  RKS
 * @since   13-sep-15.
 */

public class PalUpModule extends AbstractModule {

    final PalUpConfiguration configuration;
    final Environment environment;
    final DBI database;

    public PalUpModule(final PalUpConfiguration configuration,
                     final Environment environment,
                     final DBI database) {
        this.configuration = configuration;
        this.environment = environment;
        this.database = database;
    }

    @Override
    protected void configure() {

        // Disallow circular dependencies
        binder().disableCircularProxies();

        // Bind Configuration
        bind(PalUpConfiguration.class).toInstance(configuration);

        // Bind Resources
        bind(PalUpResource.class).asEagerSingleton();

        // Bind Database and DAOs
        bind(DBI.class).toInstance(database);
        bind(EventDAO.class).toInstance(database.onDemand(EventDAO.class));


        // Bind DataStore
        bind(DataStore.class).to(MySqlStore.class);

    }
}
