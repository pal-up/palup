package com.palup.service;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.palup.pojo.Event;
import com.palup.stores.DataStore;
import org.slf4j.LoggerFactory;

/**
 * @author  RKS
 * @since   17-sep-15.
 */

@Singleton
public class PalUpService {

    private final static org.slf4j.Logger LOGGER = LoggerFactory.getLogger(PalUpService.class);

    private DataStore dataStore;

    @Inject
    public void setDataStore(DataStore dataStore) {
        this.dataStore = dataStore;
    }

    public boolean createEvent(Event event) {
        LOGGER.info("=======SERVICE=======");

        if(dataStore.saveEvent(event) == 1) {
            return true;
        }
        return false;
    }

}
