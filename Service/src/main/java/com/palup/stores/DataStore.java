package com.palup.stores;

import com.palup.pojo.Event;

/**
 * @author  RKS
 * @since   17-sep-15
 */

public interface DataStore {
    int saveEvent(Event event);

}
