package com.palup.stores;

import com.google.inject.Inject;
import com.palup.dao.EventDAO;
import com.palup.pojo.Event;
import org.slf4j.LoggerFactory;

/**
 * @author  RKS
 * @since   17-sep-15
 */
public class MySqlStore implements DataStore {

    private final static org.slf4j.Logger LOGGER = LoggerFactory.getLogger(MySqlStore.class);

    private EventDAO eventDAO;

    @Inject
    public void setEventDAO(EventDAO eventDAO) {
        this.eventDAO = eventDAO;
    }

    public int saveEvent(Event event) {
        LOGGER.info("=======MYSQL STORE=======");

        return eventDAO.saveEvent(event.getOwnerId(), event.getTitle(), event.getDescription(), event.getLatitude(), event.getLongitude(), event.getDate());
    }
}
