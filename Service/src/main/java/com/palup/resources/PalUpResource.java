package com.palup.resources;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.palup.pojo.Event;
import com.palup.pojo.TestEvent;
import com.palup.service.PalUpService;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


/**
 * @author  RKS
 * @since   13-sep-15.
 */

@Path("/v1/palup")
@Singleton
public class PalUpResource {

    private final static org.slf4j.Logger LOGGER = LoggerFactory.getLogger(PalUpResource.class);
    private PalUpService palUpService;

    @Inject
    public void setPalUpService(PalUpService palUpService) {
        this.palUpService = palUpService;
    }

    @GET
    @Path("/hello")
    @Produces(MediaType.APPLICATION_JSON)
    public String test() throws Exception {
        return "Hello World!!";
    }

    @POST
    @Path("/event/create")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createEvent(Event event) throws Exception {
        LOGGER.info("==============");
        try {
            return Response.ok(palUpService.createEvent(event)).build();
        } catch(Exception e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("/event/test")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response testEvent(TestEvent event) throws Exception {
        LOGGER.info("==============");
        try {
            return Response.ok(event).build();
        } catch(Exception e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }
}
