create table event (
	`id` INT NOT NULL AUTO_INCREMENT,
	`ownerId` varchar(255) NOT NULL,
	`title` varchar(255) NOT NULL,
	`description` varchar(1024) NOT NULL,
	`latitude` varchar(255) NOT NULL,
	`longitude` varchar(255) NOT NULL,
	`date` varchar(255) NOT NULL,
	`created_on` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	`updated_on` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	 PRIMARY KEY (`id`)
);